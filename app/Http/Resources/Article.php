<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // this will return everything by default while calling the api at postman
        //return parent::toArray($request);

        // select and return entities in api request
        return[
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body
        ];
    }

    // return our custom data in api
    public function with($request)
    {
        return[
            'version' => '1.0.0',
            'author_url' => url('https://asheshpathak.com.np')
        ];
    }
}
// Session ko lagi

