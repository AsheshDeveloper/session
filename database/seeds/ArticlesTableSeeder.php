<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //run a factory to fill dummy data inside the table
        factory(App\Article::class, 30)->create();
    }
}
