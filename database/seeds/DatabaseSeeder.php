<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // call the required Table seeder
        $this->call(ArticlesTableSeeder::class);
    }
}
