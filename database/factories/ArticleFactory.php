<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

// define our model Article
$factory->define(App\Article::class, function (Faker $faker) {
    return [
        //dummy value to fill up our table
        'title' => $faker->text(50),
        'body' => $faker->text(200)
    ];
});
