<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API route to list Articles

// get method, route name, controller_name@with_action
Route::get('articles', 'ArticleController@index');

// API route to show a single Article
Route::get('article/{id}', 'ArticleController@show');

// API route to create new Articles
Route::post('article', 'ArticleController@store');

// API route to update an Articles
Route::put('article', 'ArticleController@store');

// API route to delete an Articles
Route::delete('article/{id}', 'ArticleController@destroy');
